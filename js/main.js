$( document ).ready(function() {
    console.log( "ready!" );

    var pull = new Audio('img/pull.mp3');
    var coinDrop = new Audio('img/coin-drop.mp3');
    var music = new Audio('img/music.mp3');
	music.volume = 0.3;
	music.loop = true;
	$(".play-music").click(function(){
		music.play();
    });
	$(".stop-music").click(function(){
		music.pause();
    });

    var stake = 50;
    var win = 0;
    var firstWheel = "";
    var secondWheel = "";
    var thirdWheel = "";
    var items = ["cherry", "lemon", "orange", "plum", "bell", "bar", "seven"];

    function spinWheel(){
    	 return items[Math.floor(Math.random() * Math.floor(7))];
    }
    function updateScore(castig){
    	stake +=  castig;
    	if (castig > 1) {
	    	$(".messagebox").text("Ai castigat $" + win);
    	}else{
	    	$(".messagebox").text("Ai pierdut");
    	}

	    $(".balance span").text(stake);
    }
    function play(){
		pull.play();
    	firstWheel = spinWheel();
    	secondWheel = spinWheel();
    	thirdWheel = spinWheel();
    	eraseReel();
    	$(".reel1 .icon").addClass(firstWheel);
    	$(".reel2 .icon").addClass(secondWheel);
    	$(".reel3 .icon").addClass(thirdWheel);
    	$(".handle").addClass("animate").delay(500).queue(function(next){
		    $(this).removeClass("animate");
		    next();
		});
    	// console.log("1: " + firstWheel + " 2: " + secondWheel + " 3: " + thirdWheel);
    	
	   	if(firstWheel === 'cherry' && secondWheel !== 'cherry'){
		    win = 2;
		    updateScore(win);
        }
	   	else if(firstWheel === 'cherry' && secondWheel === 'cherry' && thirdWheel !== 'cherry'){
		    win = 5;
		    updateScore(win);
        }
	   	else if(firstWheel === 'cherry' && secondWheel === 'cherry' && thirdWheel === 'cherry'){
		    win = 7;
		    updateScore(win);
		    coinDrop.play();
        }
	   	else if(firstWheel === 'orange' && secondWheel === 'orange' && thirdWheel === 'orange'){
		    win = 10;
		    updateScore(win);
		    coinDrop.play();
        }
	   	else if(firstWheel === 'plum' && secondWheel === 'plum' && thirdWheel === 'plum'){
		    win = 14;
		    updateScore(win);
		    coinDrop.play();
        }
	   	else if(firstWheel === 'bell' && secondWheel === 'bell' && thirdWheel === 'bell'){
		    win = 20;
		    updateScore(win);
		    coinDrop.play();
        }
	   	else if(firstWheel === 'bar' && secondWheel === 'bar' && thirdWheel === 'bar'){
		    win = 250;
		    updateScore(win);
		    coinDrop.play();
        }
	   	else if(firstWheel === 'seven' && secondWheel === 'seven' && thirdWheel === 'seven'){
		    win = 1000;
		    updateScore(win);
		    coinDrop.play();
		    $('.modal').modal('show');
        }
		else{
		    win = -1;
		    updateScore(win);
		}
	
    }
    function eraseReel(){
    	$(".icon").removeClass("cherry lemon orange plum bell bar seven");
    }

    $(".play").click(function(){
    	play();
    });
});