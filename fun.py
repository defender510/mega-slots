import random
print('''Hai sa-ti dea nenea o Pacanea!
 Ai 50 de lei. Joci cu da/nu.

For example you can answer with YEs, yEs, Y, nO, N.
To win you must get one of the following combinations:
BARA\tBARA\tBARA\t\tda\t250 lei
CLOPOT\tCLOPOT\tCLOPOT/BARa\tda\t20 lei
PRUNA\tPRUNA\tPRUNA/BARA\tda\t14 lei
PORTOCALA\tPORTOCALA\tPORTOCALA/BARA\tda\t10 lei
CIREASA\tCIREASA\tCIREASA\t\tda\t7 lei
CIREASA\tCIREASA\t  -\t\tda\t5 lei
CIREASA\t  -\t  -\t\tda\t2 lei
''')
#Constants:
INIT_STAKE = 50
ITEMS = ["CIREASA", "LAMAIE", "PORTOCALA", "PRUNA", "CLOPOT", "BARA"]

firstWheel = None
secondWheel = None
thirdWheel = None
stake = INIT_STAKE

def play():
    global stake, firstWheel, secondWheel, thirdWheel
    playQuestion = askPlayer()
    while(stake != 0 and playQuestion == True):
    # while(stake != 0):
        firstWheel = spinWheel()
        secondWheel = spinWheel()
        thirdWheel = spinWheel()
        printScore()
        playQuestion = askPlayer()

def askPlayer():
    
    global stake
    while(True):
        # answer = input("Ai lei" + str(stake) + ". Vrei sa joci? ")
        #answer = answer.lower()
        answer = "da"
        if(answer == "da" or answer == "d"):
            return True
            printScore()
        elif(answer == "nu" or answer == "n"):
            print("Ai plecat cu lei" + str(stake) + " in mana.")
            return False
        else:
            print("nu stii sa scrii?!")

def spinWheel():
    '''
    returns a random item from the wheel
    '''
    randomNumber = random.randint(0, 5)
    return ITEMS[randomNumber]

def printScore():
    '''
    prints the current score
    '''
    global stake, firstWheel, secondWheel, thirdWheel
    if((firstWheel == "CIREASA") and (secondWheel != "CIREASA")):
        win = 2
    elif((firstWheel == "CIREASA") and (secondWheel == "CIREASA") and (thirdWheel != "CIREASA")):
        win = 5
    elif((firstWheel == "CIREASA") and (secondWheel == "CIREASA") and (thirdWheel == "CIREASA")):
        win = 7
    elif((firstWheel == "PORTOCALA") and (secondWheel == "PORTOCALA") and ((thirdWheel == "PORTOCALA") or (thirdWheel == "BARA"))):
        win = 10
    elif((firstWheel == "PRUNA") and (secondWheel == "PRUNA") and ((thirdWheel == "PRUNA") or (thirdWheel == "BARA"))):
        win = 14
    elif((firstWheel == "CLOPOT") and (secondWheel == "CLOPOT") and ((thirdWheel == "CLOPOT") or (thirdWheel == "BARA"))):
        win = 20
    elif((firstWheel == "BARA") and (secondWheel == "BARA") and (thirdWheel == "BARA")):
        win = 250
    else:
        win = -1

    stake += win
    if(win > 0):
        print(firstWheel + '\t' + secondWheel + '\t' + thirdWheel + ' -- Ai castigat lei' + str(win))
    else:
        print(firstWheel + '\t' + secondWheel + '\t' + thirdWheel + ' -- Ai pierdut')

play()